# Stock trading: prediction of auction Volumes
***Challenge data by CFM***

**Student:** Lucas ANTUNES RODRIGUES

**Course:** Data Science Algorithms

| Files | Description |
| ------ | ------ |
| README.md | Repository description |
| Code.ipynb | All code used during the challenge |
| expX_Y_DESC.csv | Output sended to the challenge from experiment X.Y with models name on DESC |
| Challenge_Presentation.pdf  | PDF of the presentation uploaded on YouTube |
| .ipynb_checkpoints  | Jupyter notebook checkpoints |

# Data location provided by CFM
The data used for training and test are too large to upload here.

You can find the data here: https://challengedata.ens.fr/participants/challenges/60/

| Files to download | file name |
| ------ | ------ |
| x_train | input_training.csv |
| y_train | output_training_IxKGwDV.csv |
| x_test | input_test.csv |

**WARNING:** The name of the file **y_train** is weird but don't change it. Use it as output_training_IxKGwDV.csv. 

# YouTube Link for the presentation
https://youtu.be/W9cNJeBQ4Qc